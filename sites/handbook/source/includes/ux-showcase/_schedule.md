[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date ('21) | Host                  |
| ---------- | --------------------- |
| 2021-10-13 | APAC/Europe  (Rayana) |
| 2021-10-27 | Marcel                |
| 2021-11-10 | Rayana                |
| 2021-12-01 | Blair                 |
| 2021-12-08 | Jacki                 |

| Date ('22) | Host                  |
| ---------- | --------------------- |
| 2022-01-12 | Justin                |
| 2022-01-26 | Taurie                |
| 2022-02-09 | APAC/Europe  (Marcel) |
| 2022-02-23 | Marcel                |
| 2022-03-09 | Rayana                |
| 2022-03-23 | Blair                 |
| 2022-04-06 | Jacki                 |
| 2022-04-20 | Justin                |
| 2022-05-04 | Taurie                |
| 2022-05-18 | APAC/Europe  (Rayana) |
| 2022-06-01 | Marcel                |
| 2022-06-15 | Rayana                |
| 2022-06-29 | Blair                 |
| 2022-07-13 | Jacki                 |
| 2022-07-27 | Justin                |
| 2022-08-10 | Taurie                |
| 2022-08-24 | APAC/Europe  (Marcel) |
| 2022-09-07 | Marcel                |
| 2022-09-21 | Rayana                |
| 2022-10-05 | Blair                 |
| 2022-10-19 | Jacki                 |
| 2022-11-02 | Justin                |
| 2022-11-16 | Taurie                |
| 2022-11-30 | APAC/Europe  (Rayana) |
| 2022-12-14 | Marcel                |

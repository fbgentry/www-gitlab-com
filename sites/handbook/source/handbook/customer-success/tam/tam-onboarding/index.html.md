---
layout: handbook-page-toc
title: "TAM Onboarding"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

Onboarding for Technical Account Managers is a guided, methodical process to ensure new team members have the knowledge they need to be effective.

## Meeting shadowing

To learn how we conduct both customer meetings and internal planning meetings, a new team member will shadow these meetings with one or more established members of the team.

[Meeting Shadowing](/handbook/customer-success/tam/tam-onboarding/shadowing/)

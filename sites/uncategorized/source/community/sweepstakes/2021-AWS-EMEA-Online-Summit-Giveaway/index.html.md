---
layout: markdown_page
title: "GitLab AWS Summit Online EMEA 2021 T-Shirt Giveaway: Terms and Conditions"
description: "The AWS T-Shirt Giveaway begins on 9 June 2021 at 9:00 CET and ends on 10 June 2021 at 15:00 CET."
canonical_path: "/community/sweepstakes/2021-AWS-EMEA-Online-Summit-Giveaway/"
---

## Official Rules
PLEASE READ THESE TERMS CAREFULLY AND IN FULL. THEY CONTAIN CERTAIN CONDITIONS AND RESTRICTIONS ON THE AVAILABILITY AND USE OF THIS OFFER. VOID WHERE PROHIBITED OR RESTRICTED BY LAW.

1. OFFER DESCRIPTION: This GitLab AWS Summit Online EMEA 2021 T-Shirt Giveaway (the “Offer”) is made available by GitLab, Inc. (“GitLab”) and subject to these terms and conditions (the "Terms"). The Offer begins on 9 June 2021 at 9:00 CET and ends on 10 June 2021 at 15:00 CET. This Offer is subject to all applicable federal, state and local laws and regulations and is void where prohibited or restricted by law. Any questions, comments or problems related to the Offer should be sent by email to [amimmo@gitlab.com](mailto:amimmo@gitlab.com).
2. THE OFFER: Eligible Participants (as defined below) are entitled, subject to these Terms, to receive a one GitLab-branded T-Shirt (the “Giveaway T-Shirt”). Giveaway T-Shirts are available on a first-come-first-served basis, subject to the limits provided in Clause 3 below.
3. DURATION: The Offer is supply limited. The Offer shall end automatically and no longer be available for redemption on the earliest of:

- The expiry date and time set out above; 
- The number of Giveaway T-Shirts given away under the Offer reaching 100; and 
- The Offer being terminated by GitLab.

GitLab reserves the right to modify or terminate this Offer at any time and for any reason. GitLab shall not be obligated to permit any further attempts to take up the Offer offer after it has ended. In particular, GitLab may limit eligibility and/or duration to prevent abuse of this Offer.

4.  ELIGIBILITY: To receive this Offer, participants must satisfy all of the conditions listed at below (each an "Eligible Participant"):
5.  You must complete the GitLab AWS Summit Online EMEA 2021 T-Shirt Giveaway entry form accessible from the GitLab Partner Giveaways Page at the AWS Summit Online EMEA 2021 event, answering the question posed correctly and completing all other fields. When prompted, provide your postal address for receipt of the Giveaway T-Shirt;
6.  You must be a resident of Austria, Belgium, Czech Republic, Denmark, Finland, France, Germany, Hungary, Ireland, Italy, Netherlands, Norway, Poland, Portugal, Romania, Slovakia, Slovenia, Spain, Sweden or Switzerland.
7.  You must be a natural person and not have already participated in this Offer.
8.  REDEMPTION: If you have successfully answered the question posed and completed the entry form per Clause 4(a) and stocks of the Giveaway T-Shirt remain per Clause 3(b), you will be contacted to provide a postal address for receipt of the Giveaway T-Shirt. Ensure you provide the correct shipping address for receipt of the Giveaway T-Shirt.
9.  PRIVACY: Any personal information supplied by you will be subject to the GitLab privacy policy at [about.gitlab.com/privacy](http://about.gitlab.com/privacy/). If you wish to assert your rights to information, correction, deletion, restriction of processing, object to data processing or revoke your consent to data processing, please use our [Personal Data Request Form](https://support.gitlab.io/account-deletion/). By participating in the Offer, you grant GitLab permission to share your name, email address and postal address and any other personally identifiable information with our fulfilment partner for the purpose of fulfillment.
10.  LIMITATION OF LIABILITY: GitLab assumes no responsibility or liability for (a) any incorrect or inaccurate participation information, or for any faulty or failed electronic data transmissions; (b) any unauthorized access to, or theft, destruction or alteration of participation information at any point in the operation of this Offer; (c) any technical malfunction, failure, error, omission, interruption, deletion, defect, delay in operation or communications line failure, regardless of cause, with regard to any equipment, systems, networks, lines, satellites, servers, camera, computers or providers utilized in any aspect of the operation of the Offer; (d) inaccessibility or unavailability of any network or wireless service, the Internet or website or any combination thereof; (e) suspended or discontinued Internet, wireless or landline phone service; or (f) any injury or damage to participant's or to any other person’s computer or mobile device which may be related to or resulting from any attempt to participate in the Offer or download of any materials in the Offer. If, for any reason, the Offer is not capable of running as planned for reasons which may include without limitation, infection by computer virus, tampering, unauthorized intervention, fraud, technical failures, or any other causes which may corrupt or affect the administration, security, fairness, integrity or proper conduct of this Offer, the GitLab reserves the right at its sole discretion to cancel, terminate, modify or suspend the Offer in whole or in part. In such event, GitLab shall not have any further liability to any participant in connection with the Offer.

11. SPONSOR: GitLab, Inc., 268 Bush Street, № 350, San Francisco, CA 94104, [amimmo@gitlab.com](mailto:amimmo@gitlab.com)
